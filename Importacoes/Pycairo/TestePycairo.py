import cairo
import numpy

v = cairo.SVGSurface("example.svg", 201, 201)

f = numpy.zeros((200, 200), dtype=numpy.uint8)

frgb = 255*numpy.ones((3, 200, 200), dtype=numpy.uint8)

f[100, 100] = 255

context = cairo.Context(v)
context.scale(10, 10)
context.set_line_width(0.2)
context.move_to(0.2, 0.2)
context.line_to(8, 6)
context.stroke()
