# Unidade 1 - Seção 2.

class Line:
  def __init__(self, x1, y1, x2, y2, color):
    self.start = (x1, y1)
    self.end = (x2, y2)
    self.color = color

class VImage:
  def __init__(self):
    self.descriptor = [] # Lista vazia de descritores.
  
  def addDescriptor(self, d):
    self.descriptor.append(d)

i = VImage()
i.addDescriptor(Line(2, 2, 4, 4, 'black'))
i.addDescriptor(Line(4, 2, 2, 4, 'red'))
