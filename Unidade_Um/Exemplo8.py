# Polilinhas.

import numpy
import cv2

# Cria um vetor de polilinha. Com posições de 'x' e 'y' cada uma.
polilinhaA = numpy.array([[10,10],[20,20],[50,30],[50,40]], numpy.int32)

# Cria uma imagem matricial.
imagem_matricial = numpy.ones((400, 400), dtype = numpy.uint8)

# Desenha as linhas, com a cor branca.
cv2.polylines(imagem_matricial, [polilinhaA], True, (255,255,255))

# Mostra a imagem.
cv2.imshow('Original', imagem_matricial)
cv2.waitKey(0)
cv2.destroyAllWindows