# Rotação.

# Este exemplo move o ponto de uma posição para outra e seu
# processo é feito com matrizes.
# O tipo de movimento neste exemplo é conhecido como rotação.

import math
import numpy
import cv2

def matrizR2d(teta):
  return numpy.array([
    [math.cos(teta), -math.sin(teta), 0],
    [math.sin(teta), math.cos(teta), 0],
    [0, 0, 1]
  ])

def matrizT2d(tx, ty):
  return numpy.array([
    [1, 0, tx],
    [0, 1, ty],
    [0, 0, 1]
  ])

# Ponto de origem.
po = numpy.array([
  [4],
  [3],
  [1]
])

# Ponto central.
(cx, cy) = (1,2)

# Angulo
teta = 2

# Uma rotação de uma posição para outra não pode ser feita diretamente.
# Pode ser usada com duas translações.

# 1 - Rotacionar o 'Ponto Central' pelo lado negativo.
m = numpy.matmul(matrizR2d(teta), matrizT2d(-cx, -cy))


# 2 - Desfazer a rotação negativa, fazendo a translação 
# para o 'Ponto Central' voltar a sua posição de origem.
m = numpy.matmul(matrizT2d(cx, cy), m)

# 3 - Realizando a multiplicação das matrizes para aplicar
# a rotação correta sobre a matriz 'Ponto Origem'.
ps = numpy.matmul(m, po)

print(po)
print(ps)