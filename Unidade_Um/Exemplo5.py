# Algoritmo de desenho de circulos.

import numpy

# Desenha de Sul para Sul-Sudeste
def desenhaSSE(f, c, r, g):

  # Pega o ponto 'x' e 'y' do centro do círculo.
  (xc, yc) = c

  # Posição inicial 'x' de desenhar o círculo.
  x = 0

  # Posição inicial 'y' de desenhar o círculo.
  y = r

  # Distância de 'x', 'y' e o 'erro'.
  # As três são usadas para calcular as posições corretas para
  # realizar o desenho da curva de círculo.
  dx = 1
  dy = 2 * r - 1
  e = 0

  while(x < y):

    f[(yc+y,xc+x)] = g
    x += 1
    e += dx
    dx += 2

    if(dy < 2 * e):
      y -= 1
      e -= dy
      dy -= 2

# Desenha um círculo completo.
def desenhaCirculo(f, c, r, g):

  # Pega o ponto 'x' e 'y' do centro do círculo.
  (xc, yc) = c

  # Posição inicial 'x' de desenhar o círculo.
  x = 0

  # Posição inicial 'y' de desenhar o círculo.
  y = r

  # Distância de 'x', 'y' e o 'erro'.
  # As três são usadas para calcular as posições corretas para
  # realizar o desenho da curva de círculo.
  dx = 1
  dy = 2 * r - 1
  e = 0

  while(x < y):

    f[(yc + y, xc + x)] = g # SSE
    f[(yc - y, xc - x)] = g # NNO
    f[(yc - x, xc + y)] = g # ENE
    f[(yc + x, xc - y)] = g # OSO

    x += 1
    e += dx
    dx += 2

    if(dy < 2 * e):
      y -= 1
      e -= dy
      dy -= 2
    
    if(x > y):
      break
      
    f[(yc + x, xc + y)] = g # SSE
    f[(yc + y, xc - x)] = g # NNO
    f[(yc - x, xc - y)] = g # ENE
    f[(yc - y, xc + x)] = g # OSO

# Cria a imagem matricial.
imagem_matricial = numpy.zeros((200, 200), dtype = numpy.uint8)

# Desenha um círculo.
desenhaCirculo(imagem_matricial, [100, 100], 80, 255)

# Mostra a imagem.

import cv2

cv2.imshow("cgpiU1S2.svg", imagem_matricial)
cv2.waitKey(0)
cv2.destroyAllWindows()
