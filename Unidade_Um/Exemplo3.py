# Cairo.

# Importa a biblioteca do PyCairo.
import cairo

# Cria um arquivo SVG com nome, largura e altura da imagem.
v = cairo.SVGSurface("cgpiU1S2.svg", 201, 201)

# Cria um contexto de uma Surface. No caso, da SVGSurface
context = cairo.Context(v)

# Define a escala.
context.scale(10, 10)

# Define a largura da linha.
context.set_line_width(0.1)

# Define a sua posição inicial.
context.move_to(0.2, 0.2)

# Define a sua posição final.
context.line_to(0.8, 0.6)

# Desenha a linha com base na posição inicial e posição final.
context.stroke()

# Finaliza a renderização.
context.show_page()