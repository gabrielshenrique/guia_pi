# Translação.

import numpy

# Este exemplo move o ponto de uma posição para outra e seu
# processo é feito com matrizes.
# O tipo de movimento neste exemplo é conhecido como translação.

def matrizT2d(tx, ty):
  return numpy.array([
    [1, 0, tx],
    [0, 1, ty],
    [0, 0, 1]
  ])

# Matriz com ponto de origem. (3,2)
po = numpy.array([
  [3],
  [2],
  [1]
])

# Realiza uma multiplicação na matriz que, no final,
# acaba fazendo a movimentação do ponto.

# Transforma o ponto de 'Movimento de Translado' em uma matriz para realizar
# o cálculo com a matriz de 'Ponto de Origem'. Seu resultado é 
pt = numpy.matmul(matrizT2d(3, 6), po)

print("Ponto de partida:")
print(po)

print("\nPonto translado")
print(pt)