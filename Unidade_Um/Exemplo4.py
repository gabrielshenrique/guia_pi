# Algoritmo de desenho de retas.

import numpy

def desenhaRetaCaso1(f,p,q,g):

  # Através dos pontos P e Q, capturar o 'x' e 'y' de cada.
  (xp, yp) = p
  (xq, yq) = q

  # Calcular a inclinação da reta.
  s = float (yq-yp) / (xq-xp)

  # Para cada posição 'x', encontrar a posição 'y' e ao encontrar,
  # definir a cor nesta posição.
  for x in range(xp,xq+1):
    y = round(yp + s * (x - xp))
    
    f[(x,y)] = g
    #f[x][y] = g

# Cria a imagem matricial.
imagem_matricial = numpy.zeros((200, 200), dtype = numpy.uint8)

# Desenha a reta.
desenhaRetaCaso1(imagem_matricial, [0, 0],[199, 199], 255)

# Mostra a imagem matricial.
print(imagem_matricial)

# Mostra a imagem.

import cv2

cv2.imshow("Minha imagem", imagem_matricial)
cv2.waitKey(0)
cv2.destroyAllWindows()
