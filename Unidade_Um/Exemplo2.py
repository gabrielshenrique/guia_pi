# Unidade 1 - Seção 3.

# Numpy.

import numpy

# Cria uma imagem 200x200, toda preta. O que define ser preto é chamando o método 'zeros'.
# Onde o 0 na RGB é a cor 100% preta.
# Ou seja, NumPy cria uma imagem totalmente de zero, com tamanho 200x200,
# com seu tipo de dado ('dtype' = data type) de número com apenas 1 byte
# ('uint8' = unsigned integer de 8 bits), que vai de 0 a 255.

black_image = numpy.zeros((200,200), dtype = numpy.uint8)

# Cria uma imagem 200x200, toda branca. O que define ser branco é multiplicando '255' pelo
# retorno do método 'ones'. O 3 significa 3 canais de cores, nas quais são RGB
# (Red, Green e Blue).
# Ou seja, NumPy cria uma imagem totalmente de uma cor e definimos que esta cor seja branca,
# com seu tipo de dado ('dtype' = data type) de número com apenas 1 byte
# ('uint8' = unsigned integer de 8 bits), que vai de 0 a 255.

white_image = 255 * numpy.ones((3, 200, 200), dtype = numpy.uint8)

print(black_image)
print(white_image)